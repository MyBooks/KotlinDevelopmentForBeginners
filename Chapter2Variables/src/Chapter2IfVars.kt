fun main(args:Array<String>) {
    higherOrLower(5, 10)
}

fun higherOrLower(num1:Int, num2:Int) {
    if (num1 > num2) {
        println("$num1 is larger than $num2")
    } else if (num1 < num2) {
        println("$num2 is larger than $num1")
    } else if (num1 == num2) {
        println("$num1 is equal to $num2")
    }
}
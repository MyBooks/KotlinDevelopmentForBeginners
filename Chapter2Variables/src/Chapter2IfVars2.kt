fun main(args:Array<String>) {
    checkNumber(9)
    checkName("James")
}

fun checkNumber(num1:Int) {
    if (num1 == 8) {
        println("The number is 8")
    } else if (num1 == 9) {
        println("The number is 9")
    } else {
        println("The number is not 8")
    }
}

fun checkName(name:String) {
    if (name.equals("Bob")) {
        println("The name is Bob")
    } else {
        println("The name is not Bob, it is $name")
    }
}
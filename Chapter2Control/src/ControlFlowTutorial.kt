fun main(args:Array<String>) {
    checkTheDoor(true)
    checkAnswer('Y')
    checkAnswer2('Y')
    checkNumberRange(5)
    loopNumbers()
    checkNumber()
    matchNumber(5)
}


fun checkTheDoor(doorOpen:Boolean) {
    if (doorOpen == true) {
        println("The door is open.")
    } else {
        println("The door is closed.")
    }
}

fun checkAnswer(response:Char) {
    if( response.equals('Y')) {
        println("You have responded yes.")
    } else if (response.equals('N')) {
        println("You have responded no.")
    } else {
        println("You have given an invalid response.")
    }
}

fun checkAnswer2(response:Char) {
    when(response) {
        'Y' -> println("The response was yes2.")
        'N' -> println("The response was no2.")
        else -> println("The response was invalid2.")
    }
}

fun checkNumberRange(numToCheck:Int) {
    when(numToCheck) {
        in 1..10->println("The number $numToCheck is between 1 and 10.")
        else -> println("The number $numToCheck is not between 1 and 10.")
    }
}

fun loopNumbers() {
    for (i in 0..5) {
        println(i)
        if (i== 5) {
            println("We have reached the end!")
        }
    }
}

fun checkNumber() {
    var count = 0
    while(count < 10) {
        println("$count is less than 10")
        count++
    }
    println("$count is equal to 10")
}

fun matchNumber(num:Int) {
    var count = 0
    do {
        println("$count is not the same as $num")
        count++
    } while(num != count)
      println("$num is the same as $count")
}
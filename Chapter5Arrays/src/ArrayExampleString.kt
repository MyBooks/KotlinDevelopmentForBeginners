fun main(args:Array<String>) {
    searchArrayForName()
}

fun searchArrayForName() {
    var listOfNames = arrayOf("John", "Steve", "Tom", "Bob", "Lucas")
    var nameToFind = "Bob"
    var nameFound = false

    for (i in 0..4) {
        if(listOfNames[i].equals(nameToFind)) {
            nameFound = true
            break
        }
    }

    if (nameFound == true) {
        println("The name $nameToFind has been found.")
    } else {
        println("The name $nameToFind has not been found.")
    }
}
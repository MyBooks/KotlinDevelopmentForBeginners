fun main(args:Array<String>) {
    printlnIndexOfArray()
    printlnIndexOfArray2()
    searchArray()
}

fun printlnIndexOfArray() {
    var exampleArray = IntArray(3)

    exampleArray[0] = 15
    exampleArray[1] = 20
    exampleArray[2] = 10

    println(exampleArray[2])
    println(exampleArray)
}

fun printlnIndexOfArray2() {
    var exampleArray = intArrayOf(3,2,5,4,10,7)
    println(exampleArray[2])
}

fun searchArray() {
    var exampleArray = intArrayOf(3,2,5,4,10,7)
    var highestNumber = exampleArray[0]
    for(i in 0..5) {
        if (exampleArray[i] > highestNumber) {
            highestNumber = exampleArray[i]
        }
    }

    println("The highest number in the array is: $highestNumber")
}